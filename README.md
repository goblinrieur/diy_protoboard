# PCB for prototypes

An older version is available [here](./v0.0README.md)

Current version in v0.1

The license use is permissive one, see [LICENSE](./LICENSE) file for details.

# Goals 

- PCB can be maintained in place by screws in an external box.

- PCB design must be compatible with lower price of [JLCPCB](https://jlcpcb.com/) allowed size. _(100mmx100mm)_

- PCB must have few larger holes pads.

- PCB pads must be large enough, with high density spacing, to be able to use 0402, 0603 or 0805 SMD components on both copper & component side.

- PCB should be cuttable into a few combinations of smaller pieces.

- The SMD component packages I use the most must be available on component side. (SSOP/SOIC/SOP/SOT-23(-6)).

- Try to get it cheap, between 15-20$ would be very good.

# Looking like 

![concept_view](./modern_pb/modern_pb2.png)

![PCB_view](./modern_pb/modern_pb.png)

[PDF](./modern_pb/modern_pb.pdf)

# Gadget/bonus

I requested its production with a blue varnish on October 14, 2021. It costs only €13.75 for 10 pieces including shipment. _(15,94US$)_

- Status : Produced & Shipped 	: 2021-10-17

- Status : Received on		: 2021-11-02

[Video here : (in french)](https://www.youtube.com/watch?v=_dmHX01A6o0)

